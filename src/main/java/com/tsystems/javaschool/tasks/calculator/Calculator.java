package com.tsystems.javaschool.tasks.calculator;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Calculator {
	/**
	 * Evaluate statement represented as string.
	 *
	 * @param statement mathematical statement containing digits, '.' (dot) as
	 *                  decimal mark, parentheses, operations signs '+', '-', '*',
	 *                  '/'<br>
	 *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
	 * @return string value containing result of evaluation or null if statement is
	 *         invalid
	 */

	public String evaluate(String expression) {

		InfixToPostfix inf = new InfixToPostfix();

		String ev = inf.infixToPostfix(expression);

		EvalRPN rpn = new EvalRPN();

		Double p = rpn.evalRPN(ev);

		double newDouble = new BigDecimal(p).setScale(4, RoundingMode.HALF_DOWN).doubleValue();
        String strNewDouble = Double.toString(newDouble);
		return strNewDouble;
	}

}
