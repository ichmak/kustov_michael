package com.tsystems.javaschool.tasks.calculator;

import java.util.Stack;

public class InfixToPostfix {

	String reversePolNotation;

	public String infixToPostfix(String examp) {
		int i;
		Stack<Character> stk = new Stack<Character>();
		char temp;
		String reversePolNotation = "";

		for (i = 0; i < examp.length(); i++) {
			temp = examp.charAt(i);

			if (Character.isDigit(temp))
				reversePolNotation += temp;
			
			else if (temp == (' '))
				continue;
			else if (temp == ('.'))
				reversePolNotation += temp;
			else if (temp == '(')
				stk.push(temp);
			else if (temp == ')') {
				while (!stk.isEmpty() && stk.peek() != '(') {
					reversePolNotation += ' ';
					reversePolNotation += stk.pop();
				}
				if (stk.isEmpty())
					return "Null";
				else
					stk.pop();
			}

			else { // an operator has been encountered
				reversePolNotation += ' ';
				while (!stk.isEmpty() && (priority(temp) <= priority(stk.peek()))) {
					
					reversePolNotation += stk.pop();
					reversePolNotation += ' ';
				}
				stk.push(temp);
			}
		}
		while (!stk.isEmpty()) {
			reversePolNotation += ' ';
			reversePolNotation += stk.pop();
		}
		return reversePolNotation;
	}

	public int priority (char c) {
		switch (c) {

		case '+':
		case '-':
			return 1;
		case '*':
		case '/':
			return 2;
		}
		return -1;
	}

/*	public static void main(String[] args) {
		String examp = "22/4*2.15921";

	InfixToPostfix i = new InfixToPostfix();
		String evaluate = i.infixToPostfix(examp);
		System.out.println(evaluate);
		
	}
	*/
}
