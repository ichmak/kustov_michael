package com.tsystems.javaschool.tasks.calculator;

public class TestCalculator {

	public static void main(String[] args) {
		Calculator c = new Calculator ();
		try {
		System.out.println(c.evaluate("4 * ( 6 - 2 * (4 - 2))")); // Result: 8.0
		System.out.println(c.evaluate("22/4*2.15921")); // Result: 11.8757
		System.out.println(c.evaluate("-12)1//(")); // Result: null
	}
		catch(NullPointerException e)
        {
            System.out.print("Null");
        }
	}
}

