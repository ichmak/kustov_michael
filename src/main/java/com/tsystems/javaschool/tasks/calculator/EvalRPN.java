package com.tsystems.javaschool.tasks.calculator;

//import java.io.BufferedReader;
//import java.io.IOException;
//import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Stack;

public class EvalRPN {
	//public static void main(String[] args) throws IOException {
	
//	InfixToPostfix res = new InfixToPostfix();
	//String s = res.reversePolNotation;
		public double evalRPN(String s) {
		//BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		double r = 0;
		Double rR = new Double (r);
		for (;;) {
			//String s = in.readLine();
			if (s == null)
				break;
				

			Stack<String> tks = new Stack<String>();

			tks.addAll(Arrays.asList(s.trim().split("[ \t]+")));

			if (tks.peek().equals(""))
				continue;
			
			try {
				 rR = eval(tks);
				 
				if (!tks.empty())

					throw new Exception();

				break;
			} catch (Exception e) {
				rR = null;
				
				break;
			}
		}
		
		return rR;
	}

	private static double eval(Stack<String> tks) throws Exception {
		String tk = tks.pop();
		double result, y;
		try {
			result = Double.parseDouble(tk);
		} catch (Exception e) {
			y = eval(tks);
			result = eval(tks);
			if (tk.equals("+"))
				result += y;
			else if (tk.equals("-"))
				result -= y;
			else if (tk.equals("*"))
				result *= y;
			else if (tk.equals("/"))
				result /= y;
			else
				throw new Exception();
		}
		return result;
	}
}